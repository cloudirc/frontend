import React, { Component } from 'react';
import './App.css';

class Message extends Component {
    render() {
        return (
            <li className="Message">
                <span className="nick">{this.props.name}</span>
                <span className="text">{this.props.text}</span>
            </li>
        );
    }
}

class ChatWindow extends Component {
    render() {
        const messageElements = this.props.messages.map((message) => {
            return (
                <Message name={message.name} text={message.text} />
            );
        });

        return (
            <div className="ChatWindow">
                <ul>{messageElements}</ul>
            </div>
        );
    }
}

class ChatWindows extends Component {
    render() {
        const selectedWindow = this.props.windows.find((window) => {
            return window.name === this.props.selected;
        });

        if (selectedWindow) {
            return (
                <div className="ChatWindows">
                    <ChatWindow name={selectedWindow.name} messages={selectedWindow.messages} />
                </div>
            );
        }
        return null;
    }
}

class WindowList extends Component {
    render() {
        const channelElements = this.props.windows.map((window) => {
            return (
                <li key={window.name}>
                    <button onClick={() => this.props.onClick(window.name)}>{window.name}</button>
                </li>
            );
        });
        return (
            <div className="WindowList">
                <ul>{channelElements}</ul>
            </div>
        );
    }
}

class InputBox extends Component {
    render() {
        return (
            <input type="text" className="InputBox" />
        );
    }
}

class App extends Component {
    constructor() {
        super();
        this.state = {
            selected: ''
        }
    }

    handleClick(windowName) {
        this.setState({
            selected: windowName
        });
    }

    render() {
        const windows = [
            {name: 'Channel 1', messages: [{name: 'test1', text: 'rawr'},
                                           {name: 'test2', text: 'Rawr indeed'}]},
            {name: 'Channel 2', messages: [{name: 'test2', text: 'Rawr?"'},
                                           {name: 'test1', text: 'RAWR!'}]}
        ];

        return (
            <div className="App">
                <WindowList windows={windows} onClick={(i) => this.handleClick(i)} />
                <div className="main">
                    <ChatWindows selected={this.state.selected} windows={windows} />
                    <InputBox />
                </div>
            </div>
        );
    }
}

export default App;
